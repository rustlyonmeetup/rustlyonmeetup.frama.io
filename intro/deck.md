---
marp: true
header: Rust Lyon Meetup #10
theme: uncover
style: |
  section {
    padding:50px; /* default value is 70px */
    background: white url('img/rust_lyon_meetup_logo.png') no-repeat;
    background-position: bottom right;
    background-size: 30%;
    color: dimgray;
  }

  section.cover {
    background-position: 50% 50%;
    background-size: 100%;
    color:black;
  }

  header {
    position: absolute;
    left: 15px;
    top: 15px;
    text-align: left;
  }

  section.cover header {
    width: 100%;
    margin: auto;
    top: 50%;
    transform: translateY(-50%);
    font-size: 2.5em;
    font-weight:bold;
    text-align: center;
  }

  h1, h2 {
    margin: 0px;
  }

  h1, section.cover header {
    -webkit-text-stroke: 3px dimgray;
    color: #EEEEEE;
  }

  blockquote {
   margin: 15px auto;
   max-width: 100%;
  }

  cite {
    display: inline-block;
    text-align: right;
    width: 100%;
  }

  split {
    columns: 2;
    break-inside: avoid;
  }

  split hr {
    border:none;
    break-after: column;
    height: 0px;
  }

---
<!-- _class: cover -->

---

# Code de Conduite

- Respect, courtoisie et bienveillance
- Parler de Rust ≠ critiquer d'autres langages

---

# Débuter avec Rust

<split>

## Lire

- [Le Rust Book officiel](https://doc.rust-lang.org/book/)
- [Tuto Rust en Français](https://blog.guillaume-gomez.fr/Rust)
- [Rust by example](https://doc.rust-lang.org/rust-by-example/)
- [This week in Rust](https://this-week-in-rust.org)
- [Blessed](https://blessed.rs/)

<hr>

## Discuter

- [Salon Matrix Rust-fr](https://matrix.to/#/#rustfr:delire.party)
- [Slack LyonTechHub](lyontechhub.slack.com)

## Coder

- [Rustlings](https://github.com/rust-lang/rustlings)
- [Exercism](https://exercism.org/)

</split>

---

# Au programme ce soir

> Les traits en Rust
<cite>— <a href="https://www.linkedin.com/in/gwen-lg/">Gwendal LE GUEVEL</a></cite>

> Du MCU au cloud
<cite>— <a href="https://www.linkedin.com/in/cyrilmarpaud/">Cyril MARPAUD</a></cite>

Apéritif / Entraide / Réseautage

---

# Le matériel

- Les slides : [rustlyonmeetup.frama.io](https://rustlyonmeetup.frama.io/)
- Le dépôt : [framagit.org/rustlyonmeetup](https://framagit.org/rustlyonmeetup/rustlyonmeetup.frama.io)
- Les vidéos : [youtube.com/@RustLyonMeetup](https://www.youtube.com/@RustLyonMeetup)

---

# L'équipe

- [Cyril Couffe](https://www.linkedin.com/in/cyril-couffe-phd-331722b6/)
- [Cyril Marpaud](https://www.linkedin.com/in/cyrilmarpaud/)
- ... Toi ?

---

# <!-- fit --> Soutenez le Meetup, partagez...
- vos locaux, accueillez le meetup
- votre temps, rejoignez l'organisation
- vos compétences, proposez des talks
- vos euros, achetez à manger/boire

---

# Merci et bonne soirée !

PS : c'est l'heure de passer en mode vibreur !